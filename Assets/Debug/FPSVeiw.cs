﻿using UnityEngine;

public class FPSVeiw : MonoBehaviour
{
    float deltaTime;

    // Update is called once per frame
    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;
        GetComponent<UnityEngine.UI.Text>().text = Mathf.Ceil(fps).ToString();
    }
}
