﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevelPointBehaviour : MonoBehaviour
{
    public Action onLevelEnd;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            onLevelEnd?.Invoke();
        }
    }
}
