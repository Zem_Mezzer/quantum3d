﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

public class EnemyBehaviour : MonoBehaviour, IDamage
{

    [Header("Характеристики ИИ")]
    [SerializeField] float MoveSpeed;
    [SerializeField] int ScoreAsKillReward = 10;
    [SerializeField] Transform groundDetection;
    [Tooltip("Длинна луча вниз, определяющая коллизию с землей")]
    [SerializeField] float Distance;

    [Header("Ссылки")]
    [SerializeField] AudioClip DeathSound;
    [SerializeField] GameObject Sparks;

    [Header("Может ли ИИ убить игрока прикосновением?")]
    [SerializeField] bool CanDamagePlayerByTouch;


    bool facing = true;

    //Script will use this variables if ai cant find the ground
    bool isFlyingAI = false;
    float MoveTime;


    void Start()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, Distance))
        {
            isFlyingAI = false;
        }
        else
            isFlyingAI = true;
    }

    void Update()
    {
        Move();
    }

    void Facing()
    {
        if (facing == true)
        {
            transform.eulerAngles = new Vector3(0, -180, 0);
            transform.localScale = new Vector3(1, 1, -1);
            facing = !facing;
        }
        else
        if(facing == false)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            transform.localScale = new Vector3(1, 1, 1);
            facing = !facing;
        }
        MoveTime = 5;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player" && CanDamagePlayerByTouch)
        {
            collision.gameObject.GetComponent<CharacterController>().GetDamage();
        }
    }

    void Move()
    {
        if (!isFlyingAI)
        {
            if (!groundInfo() || WallInfo())
            {
                Facing();
            }
        }     
        if(isFlyingAI&&MoveTime<=0)
        {
            Facing();
        }

        transform.Translate(Vector2.right * MoveSpeed * Time.deltaTime);
        MoveTime -= Time.deltaTime;
    }

    bool groundInfo()
    {
        Vector3 Endpoint = new Vector3(groundDetection.position.x, groundDetection.position.y - Distance, groundDetection.position.z);
        Debug.DrawLine(groundDetection.position, Endpoint);
        return Physics.Raycast(groundDetection.position, Vector2.down, Distance);
    }

    bool WallInfo()
    {
        return Physics.CheckSphere(groundDetection.position, 0.1f, LayerMask.GetMask("Ground"));
    }

    public void GetDamage()
    {
        Instantiate(Sparks, transform.position, transform.rotation);
        ScoreManager.SMInstance.IncScore(ScoreAsKillReward);
        AudioManager.AMInstance.PlaySound(DeathSound);
        Destroy(gameObject);
    }
}
