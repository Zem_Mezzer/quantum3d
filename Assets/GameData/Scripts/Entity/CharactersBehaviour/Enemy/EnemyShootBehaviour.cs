﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootBehaviour : MonoBehaviour
{
    [SerializeField] float PlayerCheckRadius;
    [SerializeField] Transform CheckPoint;
    [SerializeField] LayerMask WhatIsPlayer;

    [Header("Если ИИ может целится")]
    [SerializeField] bool IsAimingAI;
    [SerializeField] Transform AimGun;


    //Attack Logic
    float DalayBeforeAttack;
    [Header("Логика атаки")]
    [SerializeField] Transform ShootPoint;
    [SerializeField] GameObject Bullet;


    private GameObject Player;

    private void Start()
    {
        Player = GameObject.FindWithTag("Player");
    }

    void Update()
    {
        if (PlayerCheck())
        {
            if (IsAimingAI)
            {
                AimGun.LookAt(Player.transform.position);
            }
            if (DalayBeforeAttack <= 0)
            {
                var bullet = Instantiate(Bullet, ShootPoint.position, ShootPoint.rotation);
                if (IsAimingAI) { bullet.GetComponent<EnemyBulletBehaviour>().Target = Player.transform.position; }
                DalayBeforeAttack = 5;
            }
        }
        DalayBeforeAttack -= Time.deltaTime;
    }

    bool PlayerCheck()
    {
        return Physics.CheckSphere(CheckPoint.position, PlayerCheckRadius, WhatIsPlayer);
    }
}
