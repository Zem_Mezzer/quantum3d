﻿using UnityEngine;

public class CharacterController : MonoBehaviour,IDamage
{

    [Header("Характеристики Игрока")]
    [SerializeField] float PlayerMoveSpeed;
    [SerializeField] float PlayerJumpSpeed;
    

    Animator animator;
    [Header("Ссылки")]
    [SerializeField] Joystick joystick;
    [SerializeField] AudioClip DeathSound;
    [SerializeField] AudioClip JumpSound;
    [SerializeField] GameObject Sparks;
    [SerializeField] new Rigidbody rigidbody;

    [SerializeField] Transform GroundCheck;
    [SerializeField] float GroundCheckRadius;
    [SerializeField] LayerMask WhatIsGround;


    private bool facing = true;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        Move();
        if (transform.position.y < -1)
        {
            GetDamage();
        }
    }

    void Move()
    {
        rigidbody.velocity = new Vector2(joystick.Horizontal * PlayerMoveSpeed, rigidbody.velocity.y);
        animator.SetFloat("MoveSpeed", Mathf.Abs(joystick.Horizontal));
        if (joystick.Vertical >= 0.5f)
        {
            if (isGrounded())
            {
                AudioManager.AMInstance.PlaySound(JumpSound);
                rigidbody.velocity = Vector3.up * PlayerJumpSpeed;
                animator.SetTrigger("Jump");
            }
        }

        if (joystick.Horizontal > 0 && !facing)
        {
            Facing();
        }
        else
        if (joystick.Horizontal < 0 && facing)
        {
            Facing();
        }
    }

    public void GetDamage()
    {
        Destroy(Instantiate(Sparks, transform.position, transform.rotation), 3f);
        AudioManager.AMInstance.PlaySound(DeathSound);
        GameLevelManager.GLMInstance.PlayerGetDamage();
    }

    void Facing()
    {
        facing = !facing;
        transform.Rotate(0, 180, 0);
    }

    bool isGrounded()
    {
        return Physics.CheckSphere(GroundCheck.position, GroundCheckRadius,WhatIsGround);
    }
}
