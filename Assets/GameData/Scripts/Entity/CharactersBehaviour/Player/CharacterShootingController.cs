﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterShootingController : MonoBehaviour
{
    public PlayerWeapon playerWeapon;
    public ExplosionBehaviour Explosion;

    [SerializeField] PlayerBulletBehaviour Bullet;
    [SerializeField] Transform ShootPoint;
    [SerializeField] AudioClip ShootSound;
    float ShootDalay;
    [HideInInspector] public float UltimateDelay = 0;


    [HideInInspector] ObjectPool<PlayerBulletBehaviour> BulletsPool;

    private void Start()
    {
        BulletsPool = new ObjectPool<PlayerBulletBehaviour>(Bullet);
    }

    private void Update()
    {

        ShootDalay -= Time.deltaTime;
        UltimateDelay -= Time.deltaTime;
    }

    public void Shoot()
    {

        if (ShootDalay <= 0)
        {
            var Bullet = BulletsPool.Get();
            Bullet.transform.position = ShootPoint.transform.position;
            Bullet.transform.rotation = ShootPoint.transform.rotation;
            Bullet.SetSettings(playerWeapon);
            if (Bullet.DestroyBullet == null)
            {
                Bullet.DestroyBullet = () =>
                {
                    BulletsPool.Put(Bullet);
                };
            }

            AudioManager.AMInstance.PlaySound(ShootSound);
            ShootDalay = playerWeapon.ReloadSpeed;
        }
    }

    public void Ultimate()
    {
        if (UltimateDelay <= 0)
        {
            UltimateDelay = 15;
            var ExplosionClone = Instantiate(Explosion.gameObject, transform.position, transform.rotation);
            ExplosionClone.GetComponent<ExplosionBehaviour>().IgrnoreLayer = 9;
        }
    }
}
