﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishSpawn : MonoBehaviour
{
    void Start()
    {
        Transform Finish = GameObject.FindWithTag("Finish").transform;
        Finish.position = transform.position;
    }
}
