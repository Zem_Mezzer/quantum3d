﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBehaviour : MonoBehaviour
{
    public int IgrnoreLayer;
    [SerializeField] AudioClip ExplosionSound;

    [SerializeField] SphereCollider Collider;

    private List<Collider> CollideObjects;



    private void Start()
    {
        CollideObjects = new List<Collider>();
        AudioManager.AMInstance.PlaySound(ExplosionSound);
        Invoke("DestroyCollideObjects", 0.05f);
    }

    public void SetIgnoreLayer(int Layer)
    {
        IgrnoreLayer = Layer;
    }

    private void OnTriggerEnter(Collider other)
    {
        CollideObjects.Add(other);
    }

    private void DestroyCollideObjects()
    {
        for(int i = 0; i < CollideObjects.Count; i++)
        {
            if (CollideObjects[i].gameObject.layer != IgrnoreLayer&&CollideObjects[i].gameObject.layer!=15)
            {
                if (CollideObjects[i].gameObject.layer == 9 || CollideObjects[i].gameObject.layer == 11)
                {
                    CollideObjects[i].gameObject.GetComponent<IDamage>().GetDamage();
                }
                else
                {
                    Destroy(CollideObjects[i].gameObject);
                }
            }
        }
  
        Collider.enabled = !enabled;
    }
}
