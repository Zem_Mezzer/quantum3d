﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [HideInInspector] public float Score;
    [HideInInspector] public string LevelName;
    [HideInInspector] public string FilePath;

    [SerializeField] Text score;
    [SerializeField] Text levelName;


    void Start()
    {
        Invoke("SetTextInfo", 0.1f);
    }

    void SetTextInfo()
    {
        score.text = Score.ToString();
        levelName.text = LevelName.ToString();
    }

    public void StartLevel()
    {
        GameLevelManager.MapPath = FilePath;
        GameLevelManager.MapName = LevelName;
        LoadSceneManager.LoadScene("GameLevel");
    }
}
