﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletBehaviour : MonoBehaviour
{
    [SerializeField] bool IsAimBullet;
    [HideInInspector] public Vector2 Target;

    [SerializeField] float Speed;


    //Using if Is not Aiming Bullet
    [SerializeField] new Rigidbody rigidbody;

    private void Start()
    {
        Invoke("Destroy", 10f);
    }

    void Update()
    {
        if (IsAimBullet)
        {
            rigidbody.velocity = gameObject.transform.forward * Speed;
            if(Vector3.Distance(transform.position, Target) < 0.1f)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            rigidbody.velocity = transform.right * Speed;
        }
    }


    void Destroy()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        {
            try
            {
                if(collision.gameObject.tag == "Player")
                collision.gameObject.GetComponent<CharacterController>().GetDamage();
            }
            catch
            {

            }
            Destroy();
        }
    }
}
