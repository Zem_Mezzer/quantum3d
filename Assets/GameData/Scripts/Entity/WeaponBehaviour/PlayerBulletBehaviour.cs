﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletBehaviour : MonoBehaviour
{
    [SerializeField] new Rigidbody rigidbody;
    [HideInInspector] public float Speed;
    [HideInInspector] public bool DontDestroyByCollision;
    [SerializeField] ParticleSystem BulletDestroyParticles;
    
    [HideInInspector] public Action DestroyBullet;

    [SerializeField] Renderer renderer;

    private void Update()
    {
        rigidbody.velocity = transform.right * Speed;
    }

    private void OnBecameInvisible()
    {
        Destroy();
    }

    private void Destroy()
    {
        Renderer BDMaterial = BulletDestroyParticles.GetComponent<Renderer>();
        BDMaterial.sharedMaterial.color = renderer.material.color;
        BDMaterial.sharedMaterial.SetColor("_EmissionColor", renderer.material.color);
        BDMaterial.sharedMaterial.EnableKeyword("_EMISSION");
        if (DestroyBullet != null) { DestroyBullet.Invoke(); }
    }

    private void OnDisable()
    {
        Instantiate(BulletDestroyParticles, transform.position, transform.rotation);
    }

    public void SetSettings(PlayerWeapon playerWeapon)
    {
        renderer.material.color = playerWeapon.BulletColor;
        renderer.material.SetColor("_EmissionColor", playerWeapon.BulletColor);
        renderer.material.EnableKeyword("_EMISSION");
        Speed = playerWeapon.Speed;
        DontDestroyByCollision = playerWeapon.DontDestroyByCollision;
    }

    private void OnCollisionEnter(Collision collision)
    {
        try
        {
            collision.gameObject.GetComponent<IDamage>().GetDamage();
        }
        catch { }

        if (!DontDestroyByCollision || collision.gameObject.layer == 8)
        {
            Destroy();
        }
    }
}
