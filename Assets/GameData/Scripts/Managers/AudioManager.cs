﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager AMInstance;
    [SerializeField] AudioSource audioSource;

    private void Start()
    {
        if(AMInstance == null)
        {
            AMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlaySound(AudioClip SoundToPlay)
    {
        audioSource.PlayOneShot(SoundToPlay);
    }
}
