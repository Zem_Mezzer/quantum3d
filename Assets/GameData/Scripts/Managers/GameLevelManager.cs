﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class GameLevelManager : MonoBehaviour
{
    public static string MapPath;
    public static string MapName;

    public static GameLevelManager GLMInstance;

    [SerializeField] GameObject EndLevel;

    [SerializeField] GameObject Player;

    public List<Renderer> renderers;

    public GameObject Camera;
    public GameObject PostProcessingLayer;

    void Awake()
    {
        if(GLMInstance == null)
        {
            GLMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        EndLevel.GetComponent<EndLevelPointBehaviour>().onLevelEnd = () => 
        {
            Player.SetActive(false);
            ScoreManager.SMInstance.SetScore();
            UIManager.UIInstance.LevelEnd();
        };

        SetSettings();
    }

    void SetSettings()
    {
        if (bool.Parse(PlayerPrefs.GetString("AntiAliasing", "true")))
        {
            Camera.GetComponent<PostProcessLayer>().antialiasingMode = PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
        }
        else
        {
            Camera.GetComponent<PostProcessLayer>().antialiasingMode = PostProcessLayer.Antialiasing.None;
        }
        PostProcessingLayer.SetActive(bool.Parse(PlayerPrefs.GetString("PostProcessing", "true")));
        PlayerPrefs.Save();
        
    }

    public void PlayerGetDamage()
    {
        Player.SetActive(false);
        UIManager.UIInstance.FadeIn();
        Invoke("Restart", 1.5f);
        //Show.AD;
    }


    void Restart()
    {
        LoadSceneManager.LoadScene("GameLevel");
    }

}
