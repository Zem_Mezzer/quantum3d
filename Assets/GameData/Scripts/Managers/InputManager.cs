﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public void OpenMenu()
    {
        Time.timeScale = 0;
        LoadSceneManager.LoadAdditiveScene("Menu");
    }
}
