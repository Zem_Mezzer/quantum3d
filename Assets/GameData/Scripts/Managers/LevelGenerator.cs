﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelGenerator : MonoBehaviour
{
    
    [Header("База данных объектов игры")]
    [SerializeField] ItemsDictionary[] ObjectsDatabase;
    [SerializeField] Material[] Skybox;


    void Start()
    {
        string Json = BetterStreamingAssets.ReadAllText(GameLevelManager.MapPath);
        Map map = JsonUtility.FromJson<Map>(Json);

        for (int i = 0; i < map.BlockID.Count; i++)
        {
            try { var obj = Instantiate(ObjectsDatabase[map.BlockID[i]].Prefab, map.BlockPosition[i], Quaternion.identity, transform); } catch { }
        }



        try { RenderSettings.skybox = Skybox[map.Skybox_ID]; } catch { RenderSettings.skybox = Skybox[0]; }


    }

}


[System.Serializable]
public class ItemsDictionary
{
    [Header("Имя объекта (Не обязательно)")]
    public string Name;
    public int ID;
    public GameObject Prefab;
}

public class Map
{
    public List<int> BlockID = new List<int>();
    public List<Vector3> BlockPosition = new List<Vector3>();
    public int Score;
    public int Skybox_ID;
    public string MapName;
}

