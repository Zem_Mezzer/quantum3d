﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    private void Start()
    {
        ///<summary>
        ///Frame Lock
        /// </summary>
        QualitySettings.vSyncCount = 0;
        //Application.targetFrameRate = 60;
    }
    public void OpenMainLevelsList()
    {
        LoadSceneManager.LoadAdditiveScene("MainLevelsList");
        //дин сLoadSceneManager.LoadAdditiveScene("Dark");
    }

    public void OpenSettings()
    {
        LoadSceneManager.LoadAdditiveScene("Settings");
    }

    public void OpenCustomLevelsList()
    {
        LoadSceneManager.LoadAdditiveScene("CustomLevelsList");
    }

    public void OpenTutorial()
    {
        LoadSceneManager.LoadAdditiveScene("Tutorial");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
