﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    public Toggle AntiAliasing;
    public Toggle PostProcessing;
    [SerializeField] AudioClip BackSound;

    private void Start()
    {
        AntiAliasing.isOn = bool.Parse(PlayerPrefs.GetString("AntiAliasing", "true"));
        PostProcessing.isOn = bool.Parse(PlayerPrefs.GetString("PostProcessing", "true"));
        PlayerPrefs.Save();
    }

    public void SetAntiAliasing()
    {
        PlayerPrefs.SetString("AntiAliasing", AntiAliasing.isOn.ToString());
        PlayerPrefs.Save();
    }

    public void SetPostProcessing()
    {
        PlayerPrefs.SetString("PostProcessing", PostProcessing.isOn.ToString());
        PlayerPrefs.Save();
    }
   

    public void BackToMenu()
    {
        AudioManager.AMInstance.PlaySound(BackSound);
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Settings");
    }
}
