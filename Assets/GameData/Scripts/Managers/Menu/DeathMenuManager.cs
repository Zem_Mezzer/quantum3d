﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathMenuManager : MonoBehaviour
{
    [SerializeField] AudioClip ClickSound;
    public void Respawn()
    {
        AudioManager.AMInstance.PlaySound(ClickSound);
        LoadSceneManager.LoadScene("GameLevel");
    }

    public void BackToMenu()
    {
        AudioManager.AMInstance.PlaySound(ClickSound);
        LoadSceneManager.LoadScene("MainMenu");
    }
}
