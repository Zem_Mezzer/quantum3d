﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] AudioClip ClickSound;

    public void Resume()
    {
        Time.timeScale = 1;
        AudioManager.AMInstance.PlaySound(ClickSound);
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Menu");
    } 

    public void BackToMenu()
    {
        Time.timeScale = 1;
        AudioManager.AMInstance.PlaySound(ClickSound);
        LoadSceneManager.LoadScene("MainMenu");
    }

    public void Restart()
    {
        Time.timeScale = 1;
        AudioManager.AMInstance.PlaySound(ClickSound);
        LoadSceneManager.LoadScene("GameLevel");
    }
}
