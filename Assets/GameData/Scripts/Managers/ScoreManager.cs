﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager SMInstance;
    public int Score;
    void Start()
    {
        if(SMInstance == null)
        {
            SMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public int GetScore()
    {
        return Score;
    }

    public void IncScore(int Value)
    {
        Score += Value;
    }

    public void SetScore()
    {
        if (PlayerPrefs.GetInt(GameLevelManager.MapName, 0) < Score)
        {
            PlayerPrefs.SetInt(GameLevelManager.MapName,Score);
            PlayerPrefs.Save();
        }
    }
}
