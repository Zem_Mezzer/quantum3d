﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelListManager : MonoBehaviour
{
    [SerializeField] string CurrentSceneName;
    [SerializeField] AudioClip BackSound;

    public void BackToMenu()
    {
        AudioManager.AMInstance.PlaySound(BackSound);
        SceneManager.UnloadSceneAsync(CurrentSceneName);
    }
}
