﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] string CurrentSceneName;
    [SerializeField] AudioClip BackSound;
    public void CloseTutorial()
    {
        AudioManager.AMInstance.PlaySound(BackSound);
        SceneManager.UnloadSceneAsync(CurrentSceneName);
    }
}
