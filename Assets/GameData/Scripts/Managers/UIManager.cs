﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager UIInstance;

    [Header("Ссылки на элементы интерфейса")]
    [SerializeField] Text Score;
    [SerializeField] GameObject EndLevelPanel;
    [SerializeField] GameObject Fade;

    [Header("Player")]
    [SerializeField] CharacterShootingController PlayerShootScript;
    [SerializeField] Text UltimateDelayCounter;

    private void Start()
    {
        if(UIInstance == null)
        {
            UIInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void LevelEnd()
    {
        EndLevelPanel.SetActive(true);
    }

    private void Update()
    {
        Score.text = ScoreManager.SMInstance.GetScore().ToString();
        if (PlayerShootScript.UltimateDelay > 0)
        {
            UltimateDelayCounter.gameObject.SetActive(true);
            UltimateDelayCounter.text = Convert.ToInt32(PlayerShootScript.UltimateDelay).ToString();
        }
        else
        {
            UltimateDelayCounter.gameObject.SetActive(false);
        }
    }

    public void FadeIn()
    {
        Fade.GetComponent<Animator>().SetBool("FadeIn", true);
    }
}
