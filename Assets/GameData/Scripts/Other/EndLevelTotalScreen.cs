﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndLevelTotalScreen : MonoBehaviour
{
    [SerializeField] Text Score;
    void Start()
    {
        Score.text = ScoreManager.SMInstance.GetScore().ToString();
    }

    public void EndLevel()
    {
        LoadSceneManager.LoadScene("MainMenu");
    }
}
