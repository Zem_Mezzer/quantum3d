﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Weapon", menuName ="Weapon")]
public class PlayerWeapon : ScriptableObject
{
    public float ReloadSpeed;
    public float Speed;
    public bool DontDestroyByCollision;
    public Color BulletColor;
}
