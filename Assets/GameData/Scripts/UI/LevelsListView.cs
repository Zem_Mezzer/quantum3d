﻿using UnityEngine;
using System.IO;
using System;

public class LevelsListView : MonoBehaviour
{
    [SerializeField] GameObject LevelElement;
    [SerializeField] string LevelsType;
    
    void Start()
    {
        BetterStreamingAssets.Initialize();
        int MapsCount = 0;
        string[] MapsPath = null;
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                MapsCount = BetterStreamingAssets.GetFiles("/" + LevelsType, "*.json", SearchOption.TopDirectoryOnly).Length;
                MapsPath = BetterStreamingAssets.GetFiles("/" + LevelsType, "*json", SearchOption.TopDirectoryOnly);
                break;
            case RuntimePlatform.Android:
                MapsCount = BetterStreamingAssets.ApkImpl.GetFiles("/" + LevelsType, "*.json", SearchOption.TopDirectoryOnly).Length;
                MapsPath = BetterStreamingAssets.ApkImpl.GetFiles("/" + LevelsType, "*json", SearchOption.TopDirectoryOnly);
                break;
        }
            

            for (int i = 0; i < MapsCount; i++)
            {
                var LevelEl = Instantiate(LevelElement, transform);
                LevelEl.GetComponent<LevelButton>().FilePath = MapsPath[i];
                string FilePath = MapsPath[i];

                


                string Json = BetterStreamingAssets.ReadAllText(FilePath);
                
                Map map = JsonUtility.FromJson<Map>(Json);
                string FileName = map.MapName ?? MapsPath[i];
                LevelEl.GetComponent<LevelButton>().LevelName = FileName;
                LevelEl.GetComponent<LevelButton>().Score = PlayerPrefs.GetInt(FileName,0);
            }

        
    }
}
